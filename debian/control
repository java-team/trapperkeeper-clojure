Source: trapperkeeper-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders:
 Jérôme Charaoui <jerome@riseup.net>,
 Louis-Philippe Véronneau <pollo@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk-headless,
 javahelper,
 leiningen,
 libbeckon-clojure,
 libclj-time-clojure (>= 0.15.2),
 libclojure-java,
 libcomplete-clojure <!nocheck>,
 libcore-async-clojure (>=1.3.610-4),
 libjanino-java,
 libkitchensink-clojure (>= 3.2.1),
 liblogback-java,
 libnrepl-clojure <!nocheck>,
 libprismatic-plumbing-clojure (>= 0.5.5-2),
 libprismatic-schema-clojure (>= 1.1.12),
 libpuppetlabs-i18n-clojure (>= 0.9.2-2),
 libraynes-fs-clojure (>= 1.5.2),
 libslf4j-java,
 libtools-logging-clojure,
 libtools-macro-clojure,
 libtypesafe-config-clojure,
 maven-repo-helper,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/clojure-team/trapperkeeper-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/trapperkeeper-clojure
Homepage: https://github.com/puppetlabs/trapperkeeper
Rules-Requires-Root: no

Package: libtrapperkeeper-clojure
Architecture: all
Depends:
 libbeckon-clojure,
 libclj-time-clojure (>= 0.15.2),
 libclojure-java,
 libcore-async-clojure (>= 1.3.610-4),
 libjanino-java,
 libkitchensink-clojure (>= 3.1.1-2),
 liblogback-java,
 libprismatic-plumbing-clojure (>= 0.5.5-2),
 libprismatic-schema-clojure (>= 1.1.12),
 libpuppetlabs-i18n-clojure (>= 0.9.0),
 libraynes-fs-clojure (>= 1.5.2),
 libslf4j-java,
 libtools-logging-clojure,
 libtools-macro-clojure,
 libtypesafe-config-clojure,
 ${java:Depends},
 ${misc:Depends},
Recommends:
 ${java:Recommends},
Suggests:
 libnrepl-clojure,
Breaks:
 puppetdb (<= 7.12.1-2),
 puppetserver (<= 7.9.4-2),
Description: framework for configuring, composing and running Clojure services
 Trapperkeeper is a Clojure framework for hosting long-running applications and
 services. It acts as a sort of "binder" for Ring applications and other
 modular bits of Clojure code.
